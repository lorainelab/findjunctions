# About FindJunctions

FindJunctions is a Java program that uses spliced alignments to
identify and quantify exon-exon junctions in RNA-Seq data. 

When given one or more BAM files and a corresponding sequence file (in 2bit
format), FindJunctions produces a BED file with junction features
summarizing spliced alignments in the BAM file. The score field indicates the
number of spliced alignments flanking each junction feature. 

FindJunctions uses the sequence file to infer the direction of
transcription for junction features.

This produces something similar to junctions.bed made by the old 
spliced alignmer program TopHat, but with extra options.

## Running FindJunctions 

### Required and optional parameters:


First, build the program. See below for instructions of building FindJunctions.

FindJunctions requires two parameters:

* The name of a sequence file in .2bit format (option -b,--twobit <2BIT>)
* One or more BAM files

Optional parameters include:

```
-f,--flank <FLANK>   read bases required to flank intron [default is 5]
-h,--help            print usage information and exit
-o,--output <OUT>    write output to file [optional]
-u,--unique          uniquely mapped reads only [optional]
-v,--verbose         enables debug logging [optional]
```



Use java virtual machine (JVM) option `-Xmx` to specify computer memory the java process can use when running FindJunctions.

Example invocation:

`java -Xmx1g -jar FindJunction_exe.jar -u -n 5 -b Genome.2bit -o FJ.bed sample1.bam,sample2.bam`


*Note*: An older version of FindJunction program uses -n instead of -f (flank). 


```shell
cd src/test/resources

java -Xmx1g -jar FindJunction_exe.jar -n 10 -b A_thaliana_Jun_2009.2bit -o test.bed 1.bam
```


## Build and test FindJunctions

1. Install JDK 21.
2. Install Apache mvn, required to build FindJunctions.
3. Clone this repository to your computer.
4. Build FindJunctions using maven. 
5. Once the jar is built successfully, run `runFJ.sh` to check that the software works correctly.
6. After running the shell script, you will see a test.bed file in the project root directory. Open it in IGB to see results.


### Example:

```
git clone https://bitbucket.org/lorainelab/findjunctions
cd findjunctions
mvn clean install -DskipTests=true
./runFJ.sh
```
## Visualise the data

After running FindJunctions using anyone of the above mentioned methods, visualise the data by following below steps:

1. Start IGB.
2. Select Arabidopsis thaliana as the genome.
3. Select A_thaliana_Jun_2009 as the genome version.
4. Load both the 1.bam file and the resulted test.bed file to IGB to visualise the data.

## How to contribute

Please make all changes on a branch from branch "main". After testing, submit a PR to main. 