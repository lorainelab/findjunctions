#!/bin/bash

JAR="target/find-junctions-1.0.0-jar-with-dependencies.jar"
TWOBIT="src/test/resources/A_thaliana_Jun_2009.2bit"
BAM="src/test/resources/1.bam"
FLANK="10"
OUT="test.bed"

java -Xmx1g -jar $JAR -f $FLANK -b $TWOBIT -o $OUT $BAM
