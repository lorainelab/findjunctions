# What's here

FindJunction_exe.jar:

An older version of Find Junctions. Uses older code from IGB
codebase. For regression testing.

1.bam: contains only multi-mapping reads

created using:

samtools view -hb WetDT2.mm.bam chr1:8,269,707-8,271,596 

1.bed:

created using FindJunctions_exe.jar (this directory)

java -Xmx1g -jar FindJunction_exe.jar -b A_thaliana_Jun_2009.2bit -o 1.bed 1.bam

Note: default flanking sequence length is 5

1.f_10.bed:

created using FindJunctions_exe.jar (this directory)

java -Xmx1g -jar FindJunction_exe.jar -n 10 -b A_thaliana_Jun_2009.2bit -o 1.f_10.bed 1.bam

Note: new FindJunction program will use -f (flank) instead of -n 

2.bam:

Created using:

samtools view -hb http://igbquickload.org/flower/A_thaliana_Jun_2009/BK2_processed/Ler2.sm.bam chr1:100,000-150,000 > 2.bam

Ler2.sm.bam contains uniquely-mapped RNA-Seq reads from Arabidopsis thaliana Landsberg erecta ecotype aligned onto Columbia-0 reference genome. Including this so that we will have data containing "I" and "D" values in CIGAR strings.