/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.findjunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import org.lorainelab.findjunctions.IntronCoordinateReader; 
import net.sf.samtools.CigarElement; 
import net.sf.samtools.CigarOperator; 

/**
 *
 * @author dfreese
 */
public class IntronCoordinateReaderTest {
       IntronCoordinateReader intronReader = new IntronCoordinateReader();
       List<CigarElement> testOneList = new ArrayList<>();  
       List<CigarElement> testTwoList = new ArrayList<>(); 
       List<CigarElement> testThreeList = new ArrayList<>(); 
        
       int intronStartOneBase = 100;  
       int intronEndOneBase = 0; 
       int flank = 5; 
        
        
   @Test
   public void caseOneIntron() throws Exception {
       //testOneCigarList: 70M80N70M//
       testOneList.add(new CigarElement(70,CigarOperator.M)); 
       testOneList.add(new CigarElement(80,CigarOperator.N)); 
       testOneList.add(new CigarElement(70,CigarOperator.M)); 
       
       ArrayList<int[]> finalList = new ArrayList<>(); 
       
       intronStartOneBase +=  testOneList.get(0).getLength(); 
       
       finalList = intronReader.iterateThroughCigarList(testOneList, intronStartOneBase, intronEndOneBase,flank); 
       Assert.assertEquals(170, finalList.get(0)[0]); 
       Assert.assertEquals(250, finalList.get(0)[1]); 
 
      
   }
   
   @Test
   public void caseTwoIntrons()throws Exception {
        //cigarList test case: 70M80N70M80N70M// 
        testTwoList.add(new CigarElement(70,CigarOperator.M)); 
        testTwoList.add(new CigarElement(80,CigarOperator.N)); 
        testTwoList.add(new CigarElement(70,CigarOperator.M)); 
        testTwoList.add(new CigarElement(80,CigarOperator.N)); 
        testTwoList.add(new CigarElement(70,CigarOperator.M)); 
       
        ArrayList<int[]> finalList = new ArrayList<>(); 
       
       intronStartOneBase += testTwoList.get(0).getLength(); 

       
       finalList = intronReader.iterateThroughCigarList(testTwoList, intronStartOneBase,intronEndOneBase,flank); 
           System.err.print(finalList.get(0)[0]+" " + finalList.get(0)[1] + " " + finalList.get(1)[0] + " " + finalList.get(1)[1]); 
           
           
   }
   
   @Test
   public void caseNoIntronShortFlank() throws Exception{
       //cigarList test case: 30M80N3M//
       List<CigarElement> testThreeList = new ArrayList(); 
       testThreeList.add(new CigarElement(70, CigarOperator.M)); 
       testThreeList.add(new CigarElement(80, CigarOperator.N));
       testThreeList.add(new CigarElement(3, CigarOperator.M)); 
       
       intronStartOneBase += testThreeList.get(0).getLength(); 
       ArrayList<int[]> finalList = new ArrayList<>(); 
       
       finalList = intronReader.iterateThroughCigarList(testThreeList,intronStartOneBase, intronEndOneBase, flank); 
       Assert.assertTrue(finalList.isEmpty()); 
       
   }
   @Test 
   public void caseOneIntronInsertion() throws Exception{
       //cigarList test case; 70M80N10I// 
       List<CigarElement> testFourList = new ArrayList(); 
       testFourList.add(new CigarElement(70, CigarOperator.M)); 
       testFourList.add(new CigarElement(80, CigarOperator.N)); 
       testFourList.add(new CigarElement(10, CigarOperator.I)); 
       
       intronStartOneBase += testFourList.get(0).getLength(); 
       ArrayList<int[]> finalList = new ArrayList(); 
       finalList = intronReader.iterateThroughCigarList(testFourList, intronStartOneBase, intronEndOneBase, flank); 
       
       Assert.assertEquals(170,finalList.get(0)[0]); 
       Assert.assertEquals(250,finalList.get(0)[1]); 
               
   }
   
   @Test 
   public void caseNoIntronShortInsertion(){
       //cigarList test case: 70M80N4I//
       List<CigarElement> testFiveList = new ArrayList(); 
       testFiveList.add(new CigarElement(70, CigarOperator.M)); 
       testFiveList.add(new CigarElement(80, CigarOperator.N)); 
       testFiveList.add(new CigarElement(4, CigarOperator.I)); 
       
       intronStartOneBase += testFiveList.get(0).getLength(); 
       ArrayList<int[]> finalList = new ArrayList(); 
       finalList = intronReader.iterateThroughCigarList(testFiveList, intronStartOneBase, intronEndOneBase, flank); 
       Assert.assertTrue(finalList.isEmpty());
       
   }
   
   @Test 
   public void caseMultipleIntronsShortFlank(){
       //cigarList test case: 30M80N5I80N3M//
       
       List<CigarElement> testSixList = new ArrayList(); 
       testSixList.add(new CigarElement(30,CigarOperator.M)); 
       testSixList.add(new CigarElement(80, CigarOperator.N)); 
       testSixList.add(new CigarElement(4, CigarOperator.I)); 
       testSixList.add(new CigarElement(80, CigarOperator.N)); 
       testSixList.add(new CigarElement(3, CigarOperator.M));
       
       intronStartOneBase += testSixList.get(0).getLength(); 
       ArrayList<int[]> finalList = new ArrayList(); 
       
       finalList = intronReader.iterateThroughCigarList(testSixList, intronStartOneBase, intronEndOneBase, flank); 
       Assert.assertTrue(finalList.isEmpty());   
   }
   
   
       
}
