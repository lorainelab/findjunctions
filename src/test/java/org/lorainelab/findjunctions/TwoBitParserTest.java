/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.findjunctions;

import com.google.common.collect.Lists;
import java.io.File;
import java.net.URI;
import org.biojava.nbio.genome.parsers.twobit.TwoBitParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jeckstei
 */
public class TwoBitParserTest {
   
    private TwoBitParser twoBitParser1;
    private String twoBitFile1 = "A_thaliana_Jun_2009.2bit"; // allowed?
    private TwoBitParser twoBitParser2;
    private String twoBitFile2 = "AmbiguityLowercase.2bit";
    
    /**
     * Add test cases: 
     * 
     * Should throw Exception when given chromosome name not in the
     * file. 
     * Should throw Exception when users asks for sequence using incorrect coordinates - negative
     * numbers, numbers too large. 
     * 
     * Parser should use interbase coordinates, not one-based coordinates. 
     * 
     * Parser should be able to handle lowercase letters, N ambiguity code.
     * twoBit format does not support the other codes, however.
     * See: http://www.dnabaser.com/articles/IUPAC%20ambiguity%20codes.html
     */
    
    @Before
    public void before() throws Exception {
        URI uri = this.getClass().getClassLoader().getResource(twoBitFile1).toURI();
        twoBitParser1 = new TwoBitParser(new File(uri));
        uri = this.getClass().getClassLoader().getResource(twoBitFile2).toURI();
        twoBitParser2 = new TwoBitParser(new File(uri));
    }
    
   
    @Test
    public void testLowercase() throws Exception {
        String right_answer = "aaa";
        twoBitParser2.setCurrentSequence("lowercase");
        String residues = twoBitParser2.loadFragment(4,3);
        Assert.assertEquals(right_answer, residues);
    }
    
    @Test 
    public void testAmbiguityCode() throws Exception {
        String right_answer = "NNNN";
        twoBitParser2.setCurrentSequence("ambiguity_codes");        
    }
    
    @Test
    public void testGetChromosomes() throws Exception {
        Assert.assertTrue(Lists.newArrayList(twoBitParser1.getSequenceNames()).contains("chr1"));
    }
    
    @Test
    public void testGetResidues() throws Exception {
        twoBitParser1.setCurrentSequence("chr1");
        String residues = twoBitParser1.loadFragment(1, 10);
        Assert.assertEquals("CCTAAACCCT", residues);
    }
    
    @Test
    public void testBadChromosome() throws Exception {
        Exception parser_exception = null;
        try {
            twoBitParser1.setCurrentSequence("Foo");
        }
        catch (Exception ex) {
            parser_exception = ex;
        }
        if (null == parser_exception) {
            Assert.fail("twoBit parser should throw exception if given a non-existent sequence.");
        }
    }
    
    /**
     * Look at testNG - for richer testing methods
     */
    
    

        
}
