/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.findjunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import net.sf.samtools.Cigar;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.CigarElement; 

/**
 * Contains methods for parsing intron coordinates from SAMRecord objects
 * @author Ann, Danny
 */
public class IntronCoordinateReader { 
    
    /**
     * Parse boundaries of putative introns from an alignment parsed from
     * a BAM or CRAM file.
     * @param rec - a SAMRecord parsed from a BAM or CRAM file
     * @param flank - an int 1 or larger. At least flank bases must align
     *  outside both ends of the intron.
     * @return ArrayList of two-element int arrays representing coordinates of
     * putative introns inferred from a SAMRecord object.
     */
    protected static ArrayList<int []> getIntronCoords(SAMRecord rec,int flank) {
        ArrayList<int[]> finalList = new ArrayList();

        if (rec.getCigarString().contains("N")) {
            
            List<CigarElement> cigarList = rec.getCigar().getCigarElements();

            int coordinateStart = rec.getAlignmentStart()-1;                                    
            int intronStartOneBase = (coordinateStart) + cigarList.get(0).getLength();
            int intronEndOneBase = 0;
            
            finalList = iterateThroughCigarList(cigarList,intronStartOneBase, intronEndOneBase, flank); 
        }
        return finalList;       
 
    }

    public static ArrayList<int[]> iterateThroughCigarList(List<CigarElement> cigarList, int intronStartOneBase, int intronEndOneBase,int flank){    
        ArrayList<int[]> finalList = new ArrayList(); 
        String previousCigarElement = cigarList.get(0).getOperator().toString(); 
        
        for(int i=1; i< cigarList.size()-1;i++){
            String currentCigarElement = cigarList.get(i).getOperator().toString();
            int currentCigarElementLength = cigarList.get(i).getLength(); 
            
            if(!currentCigarElement.equals("N")){
                if(previousCigarElement.equals("N")){
                    intronStartOneBase = intronEndOneBase  +  currentCigarElementLength; 
                }else if (!currentCigarElement.equals("I")){
                    intronStartOneBase += currentCigarElementLength; 
                }
            }

             if (currentCigarElement.equals("N") && (previousCigarElement.equals("M") || previousCigarElement.equals("I"))) {

                intronEndOneBase = intronStartOneBase + currentCigarElementLength;
                
                int previousCigarElementLength = cigarList.get(i-1).getLength(); 
                int nextCigarElementLength = cigarList.get(i+1).getLength();  
                
                if(previousCigarElementLength >= flank && nextCigarElementLength >= flank){
                    int[] intron = new int[2];    
                    intron[0] = intronStartOneBase;
                    intron[1] = intronEndOneBase;
                    finalList.add(intron); 
                }
            }
            previousCigarElement = currentCigarElement;
        }
        return finalList; 
    } 
}
