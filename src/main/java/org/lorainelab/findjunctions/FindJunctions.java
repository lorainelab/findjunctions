/**
 * Run FindJunction algorithm as a command-line program.
 */
package org.lorainelab.findjunctions;

import ch.qos.logback.classic.Level;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

// commons-cli-1.3.1.jar
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

// picard classes
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMRecordIterator;
import net.sf.samtools.SAMValidationError; 
import net.sf.samtools.SAMFileReader.ValidationStringency;
import net.sf.samtools.SAMRecord.SAMTagAndValue;
import net.sf.samtools.SAMTag; 
import net.sf.samtools.SAMTagUtil; 
import org.biojava.nbio.genome.parsers.twobit.TwoBitParser;

//logging
import org.slf4j.LoggerFactory;

public class FindJunctions {

    /**
     * Accepts BAM file. Identifies spliced read alignments. From these, infers
     * location of introns. Emits BED12 format where each line represents an
     * exon-exon junction feature, the boundaries of an intron. The score field
     * of the BED File indicates the number of spliced reads supporting that
     * junction.
     *
     * Uses a 2bit (sequence) file to infer junction strand.
     *
     * XXXXXX-------------XXXXXX spliced read gtxxxxxxxxxag top strand of DNA
     * junction is on plus strand, transcribed left to right >.............>
     *
     * XXXXXX-------------XXXXXX spliced read ct---------ac top strand of DNA
     * junction is on the minus strand, transcribed right to left
     * <.............<
     *
     * FindJunctions process: 0) Make hashtable to store intron feature objects
     * 1) Open BAM file(s) 2) For each line in BAM file(s), identify gaps
     * representing putative introns. Check flank length, other criteria. 3) If
     * passes, create key for putative intron: seqName:start-end 4) look up key
     * in hashtable. If not there, make new Intron with score 1. If there,
     * retrieve intron and increment score by 1. 5) Once BAMs are read, look up
     * bases at end and start of introns. Assign strand. 6) Write data to stdout
     * or file in BED12 format.
     *
     * ex) xxxxx---------xxxxxx-------xxxxxxx (alignment) A B C
     *
     * end of A and start of B define first intron (a gap) end of B and start of
     * C define the second intron
     *
     * Check that size of "xxx" in picture above is larger than -f (flank,
     * defaults to 5)
     *
     * In previous versions of FindJunctions, we added an option -t, which stood
     * for "tophat style junctions"
     *
     * In practice, we never used this option. Don't need it.
     */
    // number of bases that must be aligned on each side of the putative intron
    private static final int DEFAULT_FLANK = 5; // 

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FindJunctions.class);

    private static CommandLine parseCommandLine(String[] args) {

        setLoggingLevel(Level.INFO);

        // this was helpful
        //http://stackoverflow.com/questions/5585634/apache-commons-cli-option-type-and-default-value
        CommandLine line = null;
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption("u", "unique", false, "uniquely mapped reads only [optional]");
        options.addOption("h", "help", false, "print usage information");
        options.addOption("v", "verbose", false, "enables debug logging [optional]");
        Option option = Option.builder("b")
                .longOpt("twobit")
                .hasArg()
                .argName("2BIT")
                .required()
                .desc("2bit file with genomic sequence [required]")
                .build();
        options.addOption(option);
        option = Option.builder("o")
                .longOpt("output")
                .hasArg(true)
                .desc("write output to file [optional]")
                .numberOfArgs(1)
                .optionalArg(true)
                .argName("OUT")
                .build();
        options.addOption(option);
        option = Option.builder("f")
                .hasArg(true)
                .longOpt("flank")
                .argName("FLANK")
                .numberOfArgs(1)
                .type(Number.class)
                .desc("read bases required to flank intron [default is 5]")
                .build();
        options.addOption(option);
        String usage_string = "FindJunctions [options] FILE";
        try {
            // parse the command line arguments
            // catches most problems, like
            // when user types unsupported options
            // or enters wrong type, e.g., string instead 
            // int
            line = parser.parse(options, args);
            if (line.hasOption("flank")) {
                int flank = ((Number) line.getParsedOptionValue("flank")).intValue();
                if (flank <= 0) {
                    throw new Exception("flank must be positive integer");
                }
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            HelpFormatter f = new HelpFormatter();
            f.printHelp(usage_string, options);
            System.exit(1);
        }
        if (line.hasOption('h')) {
            HelpFormatter f = new HelpFormatter();
            f.printHelp(usage_string, options);
            System.exit(0);
        }
        /**
         * Some projects use v, vv, vvv, etc to indicate level of logging
         */
        if (line.hasOption('v')) {
            setLoggingLevel(Level.TRACE);
        }
        // there should be at least one argument
        // maybe just one? 
        // need to look at rest of code to find out
        List<String> bam_names = line.getArgList();
        if (bam_names.isEmpty()) {
            HelpFormatter f = new HelpFormatter();
            f.printHelp(usage_string, options);
            System.exit(1);
        }
        return line;
    }

    public static void main(String[] args) throws ParseException, Exception {

        CommandLine line = parseCommandLine(args);
        int flank = DEFAULT_FLANK;
        if (line.hasOption("flank")) {
            // this should success because we checked for potential
            // problems with flank in parseCommandLine
            flank = ((Number) line.getParsedOptionValue("flank")).intValue();
        }
        // if true, only consider uniquely mapped reads with flag NH option
        // 1 - NH:i:1 
        boolean unique = false;
        if (line.hasOption("unique")) {
            unique = true;
        }

        // write junction features to file or stdout
        PrintStream printer = null;
        try {
            if (line.hasOption("output")) {
                String outfile_name = line.getOptionValue("output");
                printer = new PrintStream(new FileOutputStream(outfile_name));
            } else {
                printer = System.out;
            }
        } catch (FileNotFoundException ex) {
            String outfile_name = line.getOptionValue("output");
            System.err.printf("Could not write to file: %s.", outfile_name);
            System.exit(1);
        }

        // stores intron features
        HashMap<String, Intron> introns;
        introns = new HashMap();

        // this was helpful:
        // https://www.biostars.org/p/13423/
        String[] bamNames = line.getArgs();
        for (String bamName : bamNames) { 
            try (SAMFileReader inputSam = new SAMFileReader(new File(bamName))) {
                inputSam.setValidationStringency(ValidationStringency.SILENT);
                
                try (SAMRecordIterator iter = inputSam.iterator()) {
                    while (iter.hasNext()) {
                        SAMRecord rec = iter.next();
                        if(checkValidSamRecord(rec)){
                            if (!rec.getReadUnmappedFlag()) {
                                if(unique){
                                   if(checkUniqueRead(rec)){
                                        callIntronCoordinateReader(rec,flank,introns);
                                    }
                                }else{
                                    callIntronCoordinateReader(rec,flank,introns);
                                } 
   
                            }
                        }
                    }
                }
            }
        }
        // use the twoBit file to get bases on either end of 
        // introns and figure out what strand the intron came from 
        // we'll decode the key to find out the intron start and end position
        // option -b will never be null because of how we configured Options
        String twoBitFileName = line.getOptionValue("b");
        File twoBitFileURI = new File(twoBitFileName);
        TwoBitParser sequenceReader = new TwoBitParser(twoBitFileURI);
        /**
         * Can save work by sorting introns in advance
         */
        //sequenceReader.init();
        for (Map.Entry<String, Intron> entry : introns.entrySet()) {
            String key = entry.getKey();
            Intron intron = entry.getValue();
            int start = intron.getStart();
            int end = intron.getEnd()-2;
            
            String seqName = intron.getSeqName();
            sequenceReader.setCurrentSequence(seqName);
            
            String startBases = sequenceReader.loadFragment(start, 2);
            String endBases = sequenceReader.loadFragment(end,2); 
            logger.debug(startBases);

            determineIntronStrand(startBases, endBases,intron);
            
            sequenceReader.close();
            intron.setDisplayName(key);
            
            String toPrint = BedLineBuilder.build(intron, flank);
            printer.println(toPrint);
        }
    }

    /**
     * Use the given intron coordinates to create a String that uniquely
     * identifies the location of the intron on a reference sequence named
     * seqName.
     *
     * @param coords a two-element array of ints
     * @param seqName name of reference sequence
     * @return String that uniquely identifies the intron location
     *
     * Looks like: seqname:start-end
     */
    private static String makeKeyFromCoords(int[] coords, String seqName) {
        return String.format("%s:%d-%d", seqName, coords[0], coords[1]);
    }

    private static void setLoggingLevel(Level level) {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        root.setLevel(level);
    }
    
    private static void callIntronCoordinateReader(SAMRecord rec, int flank,HashMap<String, Intron> introns){
        ArrayList<int[]> coordsList = IntronCoordinateReader.getIntronCoords(rec, flank);
        String seqName = rec.getReferenceName();
        for (Iterator<int[]> it = coordsList.iterator(); it.hasNext();) {
            int[] coords = it.next();
            String key = makeKeyFromCoords(coords, seqName);

            if (introns.containsKey(key)) {
                Intron intron = introns.get(key);
                intron.incrementScore();
            } else {
                Intron intron = new Intron(coords, seqName, 1, rec.getCigarString(), rec.getAlignmentStart());
                introns.put(key, intron);
            }
        }
    }
    
    private static boolean checkUniqueRead(SAMRecord rec){
        List<SAMTagAndValue> values = rec.getAttributes();  
        boolean uniqueReadPresent = rec.getAttributes().stream().filter( s -> s.tag.equalsIgnoreCase("NH")).anyMatch(s -> s.value.equals(1)); 
        if(uniqueReadPresent){
            return true; 
        }
        return false; 
    }
    
    private static boolean checkValidSamRecord(SAMRecord rec){
        List<SAMValidationError> errors = rec.isValid(); 
        if(errors == null){
            return true; 
        }
        return false; 
    }
    private static void determineIntronStrand(String startBases, String endBases, Intron intron){
        
       if (startBases.equals("GT") && endBases.equals("AG")) { //major spliceosome  positive check
            intron.setStrand(Intron.Strand.PLUS);
        } else if(startBases.equals("CT") && endBases.equals("GC")){ //this is the new piece of code 
            intron.setStrand(Intron.Strand.PLUS);
        }else if(startBases.equals("CT") || startBases.equals("GA") && endBases.equals("AC") || endBases.equals("TG")){ //major  poositive check 
                intron.setStrand(Intron.Strand.MINUS);
        } else if(startBases.equals("AT") || startBases.equals("GT") && endBases.equals("AC") || endBases.equals("AG")){ // minor positive check
                intron.setStrand(Intron.Strand.PLUS);                   
        } else if(startBases.equals("GT") || startBases.equals("CT") || startBases.equals("CA") || startBases.equals("GA") && 
                (endBases.equals("AT") || endBases.equals("AC") || endBases.equals("TA") || endBases.equals("TG"))){ //minor negative check     
            intron.setStrand(Intron.Strand.MINUS);   
        }else {
           logger.info("Can't determine strand {}", intron);
                intron.setStrand(Intron.Strand.UNKNOWN);
        }
    }


}
