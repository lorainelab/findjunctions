/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.findjunctions;


/**
 * Use this to make a BED format string
 * @author pi
 * 
 * See: https://genome.ucsc.edu/FAQ/FAQformat.html#format1
 */
public class BedLineBuilder {
    
    /**
     * Create and return a BED12 formatted String with the given Intron.
     * @param intron an Intron to print in BED format
     * @param flank minimum bases aligned to flanking exons
     * @return a String in BED format; no newline character one end
     */
    protected static String build(Intron intron,int flank) {
        String chrom = intron.getSeqName();
        int chromStart = intron.getStart()-flank;
        int chromEnd = intron.getEnd()+flank;
        String name=intron.getDisplayName();
        int score = intron.getScore();
        String strand = intron.getStrandAsString();
        int thickStart = chromStart;
        int thickEnd = chromStart;
        String itemRgb = "0";
        int blockCount = 2;
        String blockSizes = String.format("%d,%d",flank,flank);
        String blockStarts = String.format("0,%d",(chromEnd-flank)-chromStart);
        String to_return = String.format("%s\t%d\t%d\tJ:%s:%s\t%d\t%s\t%d\t%d\t%s\t%d\t%s\t%s",
                chrom,chromStart,chromEnd,name,strand,score,strand,thickStart,thickEnd,
                itemRgb,blockCount,blockSizes,blockStarts);
        return to_return;
    }
    
}
