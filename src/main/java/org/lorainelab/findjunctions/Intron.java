/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.findjunctions;

/**
 * Represents a score intronic region in genomic sequence. 
 * @author Ann
 */
public class Intron {
    
    private int start;
    private int end;
    private int score;
    private String seqName;
    private Strand strand;
    private String displayName;
    private String cigarString; 
    private int recCoordStart; 
    
    protected Intron(int[] coords,String seqName,int score, String cigarString, int recStart) {
        setStart(coords[0]);
        setEnd(coords[1]);
        setSeqName(seqName);
        setScore(score);
        setCigarString(cigarString); 
        setIntronRecStart(recStart); 
        
    }

    protected void setDisplayName(String name) {
        this.displayName = name;
    }
    
    protected String getDisplayName() {
        return this.displayName;
    }
    
    protected String getStrandAsString() {
        if (this.strand == Strand.PLUS) {
            return "+";
        }
        else {
            if (this.strand == Strand.MINUS) {
                return "-";
            }
            else {
                return ".";
            }
        }
    }
 
    protected void setStrand(Strand strand) {
        this.strand = strand;
    }

    private void setScore(int score) {
        this.score = score;
    }
    
    protected int getScore() {
        return this.score;
    }

    private void setSeqName(String seqName) {
        this.seqName = seqName;
    }
    
    protected String getSeqName() {
        return this.seqName;
    }
    
    protected enum Strand {
        PLUS, MINUS, UNKNOWN
    }
    
    private void setStart(int start) {
        this.start = start;
        
    }
    
    public int getStart() {
        return this.start;
    }
    
    private void setEnd(int end) {
        this.end = end;
    }
    
    public int getEnd() {
        return this.end;
    }
    
    
    public Strand getStrand() {
        return this.strand;
    }
    
    public void incrementScore() {
        this.score = this.score + 1;
    }
    public String getCigarString(){
        return cigarString; 
        
    }
    public void setCigarString(String c){
        this.cigarString = c; 
    }
    public int getIntronRecStart(){
        return this.recCoordStart; 
    }
    public void setIntronRecStart(int recStart){
        this.recCoordStart = recStart; 
        
    }
    public String toString(){
        StringBuilder str = new StringBuilder(); 
        str.append("IntronStart " + this.getStart() + "\tRecStart " + this.getIntronRecStart() + "\t" 
                + " IntronEnd " + this.getEnd() + "\tCigar " + this.getCigarString()); 
        return str.toString(); 
    }
}
